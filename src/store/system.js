const state = {
  studentData: [], //ข้อมูลนักเรียน
  studentCourse: [],
  schoolType: "",
  vocabPractice: [],
  grammarPractice: [],
  readingPractice: [],
  writingPractice: [],
  listeningPractice: [],
  schoolCode: "",
  currentLesson: undefined,
  currentPracticeLevel: 0,
  currentPracticeUnit: 1,
  currentWorld: '',
  englishInstruction: "",
  thaiInstruction: "",
  currentQuestion: "",
  totalQuestion: "",
  showLayouts: true,
  characterAction: "default",
  star: 3,
  flashcardData: [],
  innerWidth: window.innerWidth,
  innerHeight: window.innerHeight,
  passScore: '',
  star3: '',
  score: 0,
  markerList: '',
  videojs: false,
  loginKey: '',
  allowReload: true,
  vdoTeachingPath: '',
  tempReadingPath: '',
  homeButton: false,
  logoutButton: false

}
const mutations = {
  setLogoutButton: (state, payload) => {
    state.logoutButton = payload
  },
  setHomeButton: (state, payload) => {
    state.homeButton = payload
  },
  setTempReadingPath: (state, payload) => {
    state.tempReadingPath = payload
  },
  //Set ค่า studentData
  setStudentData: (state, payload) => {
    state.studentData = payload
  },
  //Set ค่า studentCourse
  setStudentCourse: (state, payload) => {
    state.studentCourse = payload
  },
  //Set ค่า schoolType
  setSchooltype: (state, payload) => {
    state.schoolType = payload
  },
  //Set แบบฝึกหัดส่วน Vocabulary
  setVocabPractice: (state, payload) => {
    state.vocabPractice = payload
  },
  //Set แบบฝึกหัดส่วน Grammar
  setGrammarPractice: (state, payload) => {
    state.grammarPractice = payload
  },
  //Set แบบฝึกหัดส่วน Reading
  setReadingPractice: (state, payload) => {
    state.readingPractice = payload
  },
  //Set แบบฝึกหัดส่วน Writing
  setWritingPractice: (state, payload) => {
    state.writingPractice = payload
  },
  setListeningPractice: (state, payload) => {
    state.listeningPractice = payload
  },
  // Set รหัส
  setSchoolCode: (state, payload) => {
    state.schoolCode = payload
  },
  // Set Lesson ปัจจุบันที่เลือก
  setCurrentLesson: (state, payload) => {
    state.currentLesson = payload
  },
  // Set ค่าเลเวลของแบบฝึกหัดปัจจุบันที่เลือกอยู่
  setCurrentPracticeLevel: (state, payload) => {
    state.currentPracticeLevel = payload
  },
  // Set ค่ายูนิตปัจจุบันที่เลือกอยู่
  setCurrentPracticeUnit: (state, payload) => {
    state.currentPracticeUnit = payload
  },
  // Set คำสั่งภาษาอังกฤษ
  setEnglishInstruction: (state, payload) => {
    state.englishInstruction = payload
  },
  // Set คำสั่งภาษาไทย
  setThaiInstruction: (state, payload) => {
    state.thaiInstruction = payload
  },
  // Set ข้อของแบบฝึกหัดที่อยู่ปัจจุบัน
  setCurrentQuestion: (state, payload) => {
    state.currentQuestion = payload
  },
  // Set จำนวนข้อของแบบฝึกหัด
  setTotalQuestion: (state, payload) => {
    state.totalQuestion = payload
  },
  // โชว์ ไม่โชว์ Layout
  setShowLayouts: (state, payload) => {
    state.showLayouts = payload
  },
  // รับค่าเปลี่ยนท่าทางของ Character
  setCharacterAction: (state, payload) => {
    state.characterAction = payload
  },
  // รับค่าเพื่อเปลี่ยนดาว
  setStar: (state, payload) => {
    state.star = payload
  },
  // เซ็ทข้อมูล Flashcard
  setFlashcard: (state, payload) => {
    state.flashcardData = payload
  },
  setInnerWidth: (state, payload) => {
    state.innerWidth = payload
  },
  setInnerHeight: (state, payload) => {
    state.innerHeight = payload
  },
  setPassScore: (state, payload) => {
    state.passScore = payload
  },
  setStar3: (state, payload) => {
    state.star3 = payload
  },
  setScore: (state, payload) => {
    state.score = payload
  },
  setMarkerList: (state, payload) => {
    state.markerList = payload
  },
  setCurrentWorld: (state, payload) => {
    state.currentWorld = payload
  },
  setVdoJs: (state, payload) => {
    state.videojs = payload
  },
  setLoginKey: (state, payload) => {
    state.loginKey = payload
  },
  setAllowReload: (state, payload) => {
    state.allowReload = payload
  },
  setVdoTeachingPath: (state, payload) => {
    state.vdoTeachingPath = payload
  }
}
const actions = {
  setLogoutButton: ({
    commit
  }, payload) => {
    commit('setLogoutButton', payload)
  },
  setHomeButton: ({
    commit
  }, payload) => {
    commit('setHomeButton', payload)
  },
  setTempReadingPath: ({
    commit
  }, payload) => {
    commit('setTempReadingPath', payload)
  },
  setStudentData: ({
    commit
  }, payload) => {
    commit('setStudentData', payload)
  },
  setStudentCourse: ({
    commit
  }, payload) => {
    commit('setStudentCourse', payload)
  },
  setSchooltype: ({
    commit
  }, payload) => {
    commit('setSchooltype', payload)
  },
  setVocabPractice: ({
    commit
  }, payload) => {
    commit('setVocabPractice', payload)
  },
  setGrammarPractice: ({
    commit
  }, payload) => {
    commit('setGrammarPractice', payload)
  },
  setReadingPractice: ({
    commit
  }, payload) => {
    commit('setReadingPractice', payload)
  },
  setWritingPractice: ({
    commit
  }, payload) => {
    commit('setWritingPractice', payload)
  },
  setListeningPractice: ({
    commit
  }, payload) => {
    commit('setListeningPractice', payload)
  },
  setSchoolCode: ({
    commit
  }, payload) => {
    commit('setSchoolCode', payload)
  },
  setCurrentLesson: ({
    commit
  }, payload) => {
    commit('setCurrentLesson', payload)
  },
  setCurrentPracticeLevel: ({
    commit
  }, payload) => {
    commit('setCurrentPracticeLevel', payload)
  },
  setCurrentPracticeUnit: ({
    commit
  }, payload) => {
    commit('setCurrentPracticeUnit', payload)
  },
  setEnglishInstruction: ({
    commit
  }, payload) => {
    commit('setEnglishInstruction', payload)
  },
  setThaiInstruction: ({
    commit
  }, payload) => {
    commit('setThaiInstruction', payload)
  },
  setCurrentQuestion: ({
    commit
  }, payload) => {
    commit('setCurrentQuestion', payload)
  },
  setTotalQuestion: ({
    commit
  }, payload) => {
    commit('setTotalQuestion', payload)
  },
  setShowLayouts: ({
    commit
  }, payload) => {
    commit('setShowLayouts', payload)
  },
  setCharacterAction: ({
    commit
  }, payload) => {
    commit('setCharacterAction', payload)
  },
  setStar: ({
    commit
  }, payload) => {
    commit('setStar', payload)
  },
  setFlashcard: ({
    commit
  }, payload) => {
    commit('setFlashcard', payload)
  },
  setInnerWidth: ({
    commit
  }, payload) => {
    commit('setInnerWidth', payload)
  },
  setInnerHeight: ({
    commit
  }, payload) => {
    commit('setInnerHeight', payload)
  },
  setPassScore: ({
    commit
  }, payload) => {
    commit('setPassScore', payload)
  },
  setStar3: ({
    commit
  }, payload) => {
    commit('setStar3', payload)
  },
  setScore: ({
    commit
  }, payload) => {
    commit('setScore', payload)
  },
  setMarkerList: ({
    commit
  }, payload) => {
    commit('setMarkerList', payload)
  },
  setCurrentWorld: ({
    commit
  }, payload) => {
    commit('setCurrentWorld', payload)
  },
  setVdoJs: ({
    commit
  }, payload) => {
    commit('setVdoJs', payload)
  },
  setLoginKey: ({
    commit
  }, payload) => {
    commit('setLoginKey', payload)
  },
  setAllowReload: ({
    commit
  }, payload) => {
    commit('setAllowReload', payload)
  },
  setVdoTeachingPath: ({
    commit
  }, payload) => {
    commit('setVdoTeachingPath', payload)
  }




}
export default {
  state,
  mutations,
  actions
}
