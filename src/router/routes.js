const routes = [
  {
    path: "/",
    component: () => import("pages/Login.vue"),
    name: "Login"
  },
  {
    path: "/test",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/Index.vue")
      }
    ]
  },
  {
    path: "/landing/:name/:practicekey",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/Landing.vue")
      }
    ]
  },
  // use normal layout
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      {
        path: "studyplan",
        component: () => import("pages/Studyplan.vue"),
        name: "studyplan"
      },
      // Trophy
      {
        path: "/trophy",
        component: () => import("pages/trophy.vue"),
        name: "trophy"
      },
      {
        path: "/trophymonster",
        component: () => import("pages/trophymonster.vue"),
        name: "trophymonster"
      },
      {
        // FLASHCARD TEMPLATE BASIC
        path: "flashcard/:practicekey",
        component: () => import("pages/Flashcard.vue"),
        name: "Flashcard"
      },
      {
        // FLASHCARD DETAILS TEMPLATE BASIC
        path: "flashcarddetails/:practicekey/:flashcardid",
        component: () => import("pages/FlashcardDetails.vue"),
        name: "flashcarddetails"
      },
      {
        // FLASHCARD TEMPLATE 2 (TEST)
        path: "flashcard2/:practicekey",
        component: () => import("pages/Flashcard_Template2.vue"),
        name: "Flashcard"
      },
      {
        path: "finishpractice",
        component: () => import("pages/FinishPractice.vue"),
        name: "finishpractice"
      },
      {
        path: "retrypage/:routename/:practicekey",
        component: () => import("pages/RetryPage.vue"),
        name: "retrypage"
      },
      {
        // VDO TEACHING
        path: "vdoteaching/:practicekey?",
        component: () => import("pages/VdoTeaching.vue"),
        name: "vdoteaching"
      },
      {
        // VDO Conversation
        path: "conversation/:practicekey?",
        component: () => import("pages/Conversation.vue"),
        name: "conversation"
      }
    ]
  },
  // user practice layout
  {
    path: "/",
    component: () => import("layouts/PracticeLayout.vue"),
    children: [
      {
        path: "matching/:practicekey",
        component: () => import("pages/Matching.vue"),
        name: "Matching"
      },
      {
        // SPELLING BEE TEMPLATE BASIC
        path: "spellingbee/:practicekey",
        component: () => import("pages/Spellingbee.vue"),
        name: "Spellingbee"
      },
      {
        // TRANSLATION TEMPLATE BASIC
        path: "translation/:practicekey",
        component: () => import("pages/translation.vue"),
        name: "translation"
      },
      {
        // DICTATION TEMPLATE BASIC
        path: "dictation/:practicekey",
        component: () => import("pages/Dictation.vue"),
        name: "dictation"
      },
      {
        // FILL IN THE BLANKS
        path: "fillintheblank/:practicekey",
        component: () => import("pages/Fillintheblank.vue"),
        name: "fillintheblank"
      },
      {
        // REAING MULTIPLE CHOICES
        path: "readingmultiple/:practicekey?",
        component: () => import("pages/ReadingMultiple.vue"),
        name: "readingmultiple"
      },
      {
        // REAING FILL IN THE BLANK
        path: "readingfillin/:practicekey?",
        component: () => import("pages/ReadingFillin.vue"),
        name: "readingfillin"
      },
      {
        // ERROR DETECTION
        path: "errordetection/:practicekey?",
        component: () => import("pages/ErrorDetection.vue"),
        name: "errordetection"
      },
      {
        // MULTIPLE CHOICES
        path: "multiplechoices/:practicekey?",
        component: () => import("pages/Multiplechoices.vue"),
        name: "multiplechoices"
      },
      {
        // Speaking
        path: "speaking/:practicekey?",
        component: () => import("pages/Speaking.vue"),
        name: "speaking"
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
