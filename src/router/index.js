import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import VueSweetalert2 from "vue-sweetalert2";
import VueGeolocation from "vue-browser-geolocation";
import VueLazyload from "vue-lazyload";
import axios from "axios";
import VueAxios from "vue-axios";
import {
  mapActions
} from "vuex";
Vue.use(VueGeolocation);
Vue.use(VueRouter);
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: "assets/errorimage.png",
  loading: "assets/loading.gif",
  attempt: 1
});
Vue.use(VueAxios, axios);
const options = {
  confirmButtonColor: "#41b882",
  cancelButtonColor: "#ff7674"
};

Vue.use(VueSweetalert2, options);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

var config = {
  apiKey: "AIzaSyAO6XZYR9cTvXCI-0orTpwlYm3Q2ys-FlY",
  authDomain: "winnerenglish-5f8d3.firebaseapp.com",
  databaseURL: "https://winnerenglish-5f8d3.firebaseio.com",
  projectId: "winnerenglish-5f8d3",
  storageBucket: "winnerenglish-5f8d3.appspot.com",
  messagingSenderId: "40516029824"
};

firebase.initializeApp(config);
export const db = firebase.firestore();
export const storage = firebase.storage().ref();
db.settings({
  timestampsInSnapshots: true
});

Vue.mixin({
  data() {
    return {
      displayDiamondMixin: 0,
      version: "2.2.8",
      correctSoundEffect: new Audio("../statics/correct.mp3"),
      wrongSoundEffect: new Audio("../statics/wrong.mp3")
    };
  },
  methods: {
    ...mapActions(["setStudentData", "setLoginKey", "setTempReadingPath"]),
    trophyBtn() {
      this.$router.push("/trophy");
    },
    logout() {
      this.$q
        .dialog({
          title: "ออกจากระบบ",
          message: "คุณต้องการออกจากระบบนี้ใช่หรือไม่",
          ok: {
            push: true
          },
          cancel: {
            color: "negative"
          },
          persistent: true
        })
        .onOk(() => {
          if (document.exitFullscreen) {
            document.exitFullscreen();
          } else if (document.mozCancelFullScreen) {
            /* Firefox */
            document.mozCancelFullScreen();
          } else if (document.webkitExitFullscreen) {
            /* Chrome, Safari and Opera */
            document.webkitExitFullscreen();
          } else if (document.msExitFullscreen) {
            /* IE/Edge */
            document.msExitFullscreen();
          }
          db.collection("school")
            .doc(this.$store.state.system.schoolCode)
            .collection("monitor")
            .doc(this.$store.state.system.studentData.key)
            .update({
              status: "offline"
            });
          this.setStudentData([]);

          this.$router.push("/");
        });
    },
    showLoading() {
      this.$q.loading.show({
        delay: 400
      });
    },
    hideLoading() {
      // setTimeout(() => {
      this.$q.loading.hide();
      // }, 500);
    },
    async savePracticeLog(
      practicekey,
      score,
      status,
      star,
      world,
      practicename,
      order,
      practicetype
    ) {
      // console.log(practicekey, score, status, star, world, practicename, order, practicetype);
      let currentTerm = this.$store.state.system.studentCourse[
        this.$store.state.system.currentLesson
      ].academicYear;
      currentTerm = currentTerm.replace("/", "-");
      let practiceLogRef = await db
        .collection("school")
        .doc(this.$store.state.system.schoolCode)
        .collection("student")
        .doc(this.$store.state.system.studentData.key)
        .collection("practicelog")
        .doc(currentTerm)
        .collection("practicescore");

      let practice = await practiceLogRef
        .where("practicekey", "==", practicekey)
        .where("status", "==", "pass")
        .get();
      if (practice.empty) {
        let currentTermWithUnderScore = currentTerm.replace(/-/g, "_");

        //  ex. 100_1_2562 ใช้เข้าหา doc ใน collection academicyar
        let academicTermRef =
          this.$store.state.system.schoolCode + "_" + currentTermWithUnderScore;

        let academicRef = await db
          .collection("academicyear")
          .doc(academicTermRef)
          .get();
        var startDate = academicRef.data().startdate;
        startDate = startDate.replace("/", "-").replace("/", "-");
        let WeekApi =
          "https://api.winner-english.com/data/api/checkweek.php?cdate=" +
          startDate;

        let getWeek = await axios.get(WeekApi);

        let startWeek = getWeek.data;

        let api = "https://api.winner-english.com/data/api/gettime.php";

        let response = await axios.get(api);

        let date = response.data[0].date;
        // let week = response.data[0].week
        let microtime = response.data[0].microtime;
        let year = date.split("/");
        year = year[2];

        let lastTimeRef = await db
          .collection("school")
          .doc(this.$store.state.system.schoolCode)
          .collection("student")
          .doc(this.$store.state.system.studentData.key)
          .get();
        let lastTime = lastTimeRef.data().lastActiveTime;
        // เวลาที่ใช้ในการทำแบบฝึกหัด เกิดจากการคำนวณจากการ ใช้เวลาปัจจุบันจาก server ลบกับ เวลาล่าสุดที่เข้าแบบฝึกหัด
        let useTime = microtime - lastTime;
        // Convert เป็นค่าวินาที
        useTime = useTime / 1000;

        console.log(useTime);

        practiceLogRef.add({
          level: Number(this.$store.state.system.currentPracticeLevel),
          unit: this.$store.state.system.currentPracticeUnit,
          practicekey: practicekey,
          score: score,
          star: star,
          status: status,
          world: world,
          practiceName: practicename,
          order: order,
          practicetype: practicetype,
          date: date,
          week: startWeek,
          useTimeInSecond: useTime // เวลาที่ใช้ในการทำแบบฝึกหัด
        });
        if (
          status == "pass" &&
          practicetype != "Flashcard" &&
          practicetype != "Teaching VDO" &&
          practicetype != "Conversation VDO" &&
          practicetype != "VDO Teaching"
        ) {
          this.updateDiamondAndStar();
          this.saveStudentProgress(world);
        }
      }
    },
    saveStudentProgress(world) {
      var term = this.$store.state.system.studentCourse[
        this.$store.state.system.currentLesson
      ].academicYear;
      term = term.replace("/", "-");
      var unit = "unit" + this.$store.state.system.currentPracticeUnit;
      db.collection("school")
        .doc(this.$store.state.system.schoolCode)
        .collection("student")
        .doc(this.$store.state.system.studentData.key)
        .collection("progress")
        .doc(term)
        .get()
        .then(doc => {
          if (!doc.exists) {
            // กรณียังไม่เคยมีการบันทึก Progress ให้เพิ่มครั้งแรก
            let vocab = 0;
            let grammar = 0;
            let reading = 0;
            let writing = 0;
            let listening = 0;
            if (world == "Vocabulary") {
              vocab = 1;
            } else if (world == "Grammar") {
              grammar = 1;
            } else if (world == "Reading") {
              reading = 1;
            } else if (world == "Writing") {
              writing = 1;
            } else if (world == "Listening") {
              listening = 1;
            }
            db.collection("school")
              .doc(this.$store.state.system.schoolCode)
              .collection("student")
              .doc(this.$store.state.system.studentData.key)
              .collection("progress")
              .doc(term)
              .set({
                academic: this.$store.state.system.studentCourse[
                  this.$store.state.system.currentLesson
                ].academicYear,
                vocabulary: vocab,
                grammar: grammar,
                reading: reading,
                writing: writing,
                listening: listening
              });

            db.collection("school")
              .doc(this.$store.state.system.schoolCode)
              .collection("student")
              .doc(this.$store.state.system.studentData.key)
              .collection("progress")
              .doc(term)
              .collection(unit)
              .doc("progress")
              .set({
                academic: this.$store.state.system.studentCourse[
                  this.$store.state.system.currentLesson
                ].academicYear,
                vocabulary: vocab,
                grammar: grammar,
                reading: reading,
                writing: writing,
                listening: listening
              });
          } else {
            let vocab = doc.data().vocabulary;
            let grammar = doc.data().grammar;
            let reading = doc.data().reading;
            let writing = doc.data().writing;
            let listening = doc.data().listening;
            if (world == "Vocabulary") {
              vocab += 1;
            } else if (world == "Grammar") {
              grammar += 1;
            } else if (world == "Reading") {
              reading += 1;
            } else if (world == "Writing") {
              writing += 1;
            } else if (world == "Listening") {
              listening += 1;
            }

            db.collection("school")
              .doc(this.$store.state.system.schoolCode)
              .collection("student")
              .doc(this.$store.state.system.studentData.key)
              .collection("progress")
              .doc(term)
              .update({
                academic: this.$store.state.system.studentCourse[
                  this.$store.state.system.currentLesson
                ].academicYear,
                vocabulary: vocab,
                grammar: grammar,
                reading: reading,
                writing: writing,
                listening: listening
              });

            db.collection("school")
              .doc(this.$store.state.system.schoolCode)
              .collection("student")
              .doc(this.$store.state.system.studentData.key)
              .collection("progress")
              .doc(term)
              .collection(unit)
              .doc("progress")
              .get()
              .then(docunit => {
                // update progress ภายใน unit นั้นๆ
                if (docunit.exists) {
                  let vocab = docunit.data().vocabulary;
                  let grammar = docunit.data().grammar;
                  let reading = docunit.data().reading;
                  let writing = docunit.data().writing;
                  let listening = docunit.data().listening;
                  if (world == "Vocabulary") {
                    vocab += 1;
                  } else if (world == "Grammar") {
                    grammar += 1;
                  } else if (world == "Reading") {
                    reading += 1;
                  } else if (world == "Writing") {
                    writing += 1;
                  } else if (world == "Listening") {
                    listening += 1;
                  }

                  db.collection("school")
                    .doc(this.$store.state.system.schoolCode)
                    .collection("student")
                    .doc(this.$store.state.system.studentData.key)
                    .collection("progress")
                    .doc(term)
                    .collection(unit)
                    .doc("progress")
                    .update({
                      academic: this.$store.state.system.studentCourse[
                        this.$store.state.system.currentLesson
                      ].academicYear,
                      vocabulary: vocab,
                      grammar: grammar,
                      reading: reading,
                      writing: writing,
                      listening: listening
                    });
                } else {
                  // กรณียังไม่เคยบันทึกใน unit นี้
                  let vocab = 0;
                  let grammar = 0;
                  let reading = 0;
                  let writing = 0;
                  let listening = 0;

                  if (world == "Vocabulary") {
                    vocab = 1;
                  } else if (world == "Grammar") {
                    grammar = 1;
                  } else if (world == "Reading") {
                    reading = 1;
                  } else if (world == "Writing") {
                    writing = 1;
                  } else if (world == "Listening") {
                    listening = 1;
                  }

                  db.collection("school")
                    .doc(this.$store.state.system.schoolCode)
                    .collection("student")
                    .doc(this.$store.state.system.studentData.key)
                    .collection("progress")
                    .doc(term)
                    .collection(unit)
                    .doc("progress")
                    .set({
                      academic: this.$store.state.system.studentCourse[
                        this.$store.state.system.currentLesson
                      ].academicYear,
                      vocabulary: vocab,
                      grammar: grammar,
                      reading: reading,
                      writing: writing,
                      listening: listening
                    });
                }
              });
          }
        });
    },
    updateDiamondAndStar() {
      db.collection("school")
        .doc(this.$store.state.system.schoolCode)
        .collection("student")
        .doc(this.$store.state.system.studentData.key)
        .get()
        .then(doc => {
          var newStar;
          if (doc.data().star) {
            newStar = doc.data().star + this.$store.state.system.star;
          } else {
            newStar = this.$store.state.system.star;
          }
          var newDiamond;
          if (doc.data().diamond) {
            newDiamond = doc.data().diamond + this.$store.state.system.star;
          } else {
            newDiamond = this.$store.state.system.star;
          }

          db.collection("school")
            .doc(this.$store.state.system.schoolCode)
            .collection("student")
            .doc(this.$store.state.system.studentData.key)
            .update({
              diamond: newDiamond,
              star: newStar
            });
        });
    },
    playSound(file, volume) {
      if (file) {
        var audio = new Audio(file);
        audio.volume = volume;
        audio.play();
      }
    },
    shuffle(a) {
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    },
    async saveTimeProgress() {
      let api = "https://api.winner-english.com/data/api/gettime.php";
      let response = await axios.get(api);
      let microtime = response.data[0].microtime;
      db.collection("school")
        .doc(this.$store.state.system.schoolCode)
        .collection("student")
        .doc(this.$store.state.system.studentData.key)
        .update({
          lastActiveTime: microtime
        });
    },
    async saveMonitor(practicekey, expectminutes) {
      // ระดับชั้น/ห้อง ,
      let _this = this;
      var levelUnit =
        "L" +
        _this.$store.state.system.currentPracticeLevel +
        "U" +
        _this.$store.state.system.currentPracticeUnit;
      let currentWorld = _this.$store.state.system.currentWorld;

      let api = "https://api.winner-english.com/data/api/gettime.php";
      let response = await axios.get(api);
      let microtime = response.data[0].microtime;
      let term = this.$store.state.system.studentCourse[
        this.$store.state.system.studentCourse.length - 1
      ].academicYear;
      term = term.replace("/", "-");
      let world;
      this.saveTimeProgress();

      if (currentWorld == 1) {
        world = "Vocabulary";
      } else if (currentWorld == 2) {
        world = "Grammar";
      } else if (currentWorld == 3) {
        world = "Reading";
      } else if (currentWorld == 4) {
        world = "Writing";
      } else if (currentWorld == 5) {
        world = "Listening";
      }
      var expectTime = expectminutes * 60 * 1000 + Number(microtime);

      if (practicekey != "studyplan") {
        let practiceDB = await db
          .collection("practicelist")
          .doc("data")
          .collection(levelUnit)
          .doc(practicekey)
          .get();
        let data = {
          world: world,
          level: _this.$store.state.system.currentPracticeLevel,
          unit: _this.$store.state.system.currentPracticeUnit,
          practiceName: practiceDB.data().name,
          practiceType: practiceDB.data().practicetype,
          name: _this.$store.state.system.studentData.name,
          surname: _this.$store.state.system.studentData.surname,
          status: "online",
          lastPracticeTime: microtime,
          room: _this.$store.state.system.studentCourse[
            _this.$store.state.system.studentCourse.length - 1
          ].room,
          studentno: _this.$store.state.system.studentCourse[
            _this.$store.state.system.studentCourse.length - 1
          ].studentno,
          practicekey: practicekey,
          expect: expectTime,
          term: term
        };

        db.collection("school")
          .doc(_this.$store.state.system.schoolCode)
          .collection("monitor")
          .doc(_this.$store.state.system.studentData.key)
          .set(data);
      } else {
        let data = {
          world: "-",
          level: "-",
          unit: "-",
          practiceName: "-",
          practiceType: "หน้าหลัก",
          name: _this.$store.state.system.studentData.name,
          surname: _this.$store.state.system.studentData.surname,
          status: "online",
          lastPracticeTime: microtime,
          room: _this.$store.state.system.studentCourse[
            _this.$store.state.system.studentCourse.length - 1
          ].room,
          studentno: _this.$store.state.system.studentCourse[
            _this.$store.state.system.studentCourse.length - 1
          ].studentno,
          practicekey: "-",
          expect: expectTime,
          term: term
        };

        db.collection("school")
          .doc(_this.$store.state.system.schoolCode)
          .collection("monitor")
          .doc(_this.$store.state.system.studentData.key)
          .set(data);
      }
    },
    displayDiamond() {
      db.collection("school")
        .doc(this.$store.state.system.schoolCode)
        .collection("student")
        .doc(this.$store.state.system.studentData.key)
        .onSnapshot(doc => {
          if (doc.data().diamond) {
            this.displayDiamondMixin = doc.data().diamond;
          }
        });
    },

    checkLoginKey() {
      db.collection("school")
        .doc(this.$store.state.system.schoolCode)
        .collection("student")
        .doc(this.$store.state.system.studentData.key)
        .onSnapshot(doc => {
          // console.log(doc.data().loginKey, this.$store.state.system.studentData.key);
          if (doc.data().loginKey != this.$store.state.system.loginKey) {
            this.setLoginKey(doc.data().loginKey);
            this.$q
              .dialog({
                title: "แจ้งเตือน",
                message: "มีการเข้าสู่ระบบจากเครื่องอื่น",
                persistent: true
              })
              .onOk(() => {
                this.setStudentData([]);
                this.$router.push("/");
              });
          }
        });
    },
    backToHome() {
      let routeName = this.$route.name;
      this.$q
        .dialog({
          title: "แจ้งเตือน",
          message: "คุณต้องการกลับสู่หน้าหลักใช่หรือไม่",
          ok: {
            push: true
          },
          cancel: {
            color: "negative"
          },
          persistent: true
        })
        .onOk(() => {
          this.$router.push("/studyplan");
          if (routeName == "readingmultiple" || routeName == "readingfillin") {
            let tempPath = this.$store.state.system.tempReadingPath;
            this.playReading(tempPath, false);
          }
        });
    },
    // Code ป้องกันในกรณีเน็ตช้า แล้ว user ทะลุเข้าไปหน้าแบบฝึกหัดได้ เช็คว่าเคยทำแบบฝึกหัดผ่านแล้วหรือยัง ถ้าทำแล้วให้เด้งกลับมาหน้า Studyplan
    checkPracticePermission(practicekey) {
      let term = this.$store.state.system.studentCourse[
        this.$store.state.system.currentLesson
      ].academicYear;
      term = term.replace("/", "-");
      db.collection("school")
        .doc(this.$store.state.system.schoolCode)
        .collection("student")
        .doc(this.$store.state.system.studentData.key)
        .collection("practicelog")
        .doc(term)
        .collection("practicescore")
        .where("practicekey", "==", practicekey)
        .where("status", "==", "pass")
        .get()
        .then(doc => {
          if (doc.empty == false) {
            this.$router.push("/studyplan");
          }
        });
    },
    checkSession() {
      if (this.$store.state.system.studentData.length == 0) {
        // console.log(this.$store.state.system.studentData);
        // console.log("FAILED");
        this.$router.push("/");
      }
    },
    // หลีกเลี่ยงคำศัพท์เสริม
    preventExtraword(word) {
      if (
        word != "span" &&
        word != "onclick" &&
        word != "class" &&
        word != "play" &&
        word != "new" &&
        word != "audio" &&
        word != "text" &&
        word != "teal" &&
        word != "tooltip" &&
        word != "data" &&
        word != "tool" &&
        word != "type" &&
        word != "true"
      ) {
        return 1;
      } else {
        return 2;
      }
    },
    playReading(path, mode) {
      // console.log(path);
      // this.tempReadingPath = path;
      this.setTempReadingPath(path);
      if (mode) {
        path.play();
      } else {
        path.currentTime = 0;
        path.pause();
      }
    }
  }
});

export default function ( /* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      y: 0
    }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
